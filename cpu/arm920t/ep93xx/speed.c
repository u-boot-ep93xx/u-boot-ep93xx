/* vim: set ts=8 sw=8 noet:
 *
 * Cirrus Logic EP93xx PLL support.
 *
 * Copyright (C) 2004, 2005
 * Cory T. Tusar, Videon Central, Inc., <ctusar@videon-central.com>
 *
 * Based on the S3C24x0 speed.c, which is
 *
 * (C) Copyright 2001-2002
 * Wolfgang Denk, DENX Software Engineering, <wd@denx.de>
 *
 * (C) Copyright 2002
 * David Mueller, ELSOFT AG, <d.mueller@elsoft.ch>
 *
 * See file CREDITS for list of people who contributed to this project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <common.h>
#include <ep93xx.h>


#define MPLL 0
#define UPLL 1

/* ------------------------------------------------------------------------- */
/* NOTE: This describes the proper use of this file.
 *
 * CONFIG_SYS_CLK_FREQ should be defined as the input frequency of the PLL.
 *
 * get_FCLK(), get_HCLK(), get_PCLK() and get_UCLK() return the clock of
 * the specified bus in HZ.
 */
/* ------------------------------------------------------------------------- */

static ulong get_PLLCLK(int pllreg)
{
	/* to be implemented */
	return 0;
}

/* return FCLK frequency */
/* the parameter here is to force lookup on trunk 0 or trunk 1 of the clock tree
 * independent from what the chip is programmed to. to just get FCLK pass in something other then 0
 */
ulong get_FCLK(int trunk)
{
	/* to be implemented */
	return 0;
}


/* return HCLK frequency */
ulong get_HCLK(void)
{
	/* to be implemented */
	return 0;
}

ulong get_UART_PCLK(void)
{
	/* to be implemented */
	return 0;
}

ulong get_TIMER_PCLK(void)
{
	/* to be implemented */
	return 0;
}
