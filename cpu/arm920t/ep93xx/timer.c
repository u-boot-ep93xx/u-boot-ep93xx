/* vim: set ts=8 sw=8 noet:
 *
 * Cirrus Logic EP93xx interrupt support.
 *
 * Copyright (C) 2004, 2005
 * Cory T. Tusar, Videon Central, Inc., <ctusar@videon-central.com>
 *
 * Based on the original intr.c Cirrus Logic EP93xx Rev D. interrupt support,
 * author unknown.
 *
 * See file CREDITS for list of people who contributed to this project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <common.h>

#if defined(CONFIG_EP93XX)
#include <linux/types.h>
#include <ep93xx.h>



int timer_init(void);
void reset_timer(void);
ulong get_timer(ulong base);
void set_timer(ulong t);
void udelay(unsigned long usec);
void reset_timer_masked(void);
ulong get_timer_masked(void);
void udelay_masked(unsigned long usec);
unsigned long long get_ticks(void);
ulong get_tbclk(void);


int timer_load_val = 0;

/* macro to read the 16 bit timer */
static inline ulong READ_TIMER(void)
{
	return(Timer1Value & 0xffff);
}

static ulong timestamp;
static ulong lastdec;


/**
 * Initialize interrupt support.
 */
int timer_init(void)
{
	Timer1Control = 0x00; /* use timer 1 with 2KHz and free running */
	if (timer_load_val == 0) {
		/*
		 * for 10 ms clock period @ PCLK with 4 bit divider = 1/2
		 * (default) and prescaler = 16. Should be 10390
		 * @33.25MHz and 15625 @ 50 MHz
		 */
		timer_load_val = 21; /* set to constant just now, until I resolve clocking issues */
	}
	/* auto load, manual update of Timer 1 */
	lastdec = Timer1Load = timer_load_val;
	Timer1Control = 0xC0; /* Enable the timer and periodic mode*/

	return(0);
}

/*
 * timer without interrupts
 */

void reset_timer(void)
{
	reset_timer_masked();
}

ulong get_timer(ulong base)
{
	return get_timer_masked() - base;
}

void set_timer(ulong t)
{
	timestamp = t;
}

void udelay(unsigned long usec)
{
	ulong tmo;

	tmo = usec / 1000;
	tmo *= (timer_load_val * 100);
	tmo /= 1000;

	tmo += get_timer(0);

	while (get_timer_masked() < tmo)
		/*NOP*/;
}

void reset_timer_masked(void)
{
	/* reset time */
	lastdec = READ_TIMER();
	timestamp = 0;
}

ulong get_timer_masked(void)
{
	ulong now = READ_TIMER();

	if (lastdec >= now) {
		/* normal mode */
		timestamp += lastdec - now;
	} else {
		/* we have an overflow ... */
		timestamp += lastdec + timer_load_val - now;
	}
	lastdec = now;

	return timestamp;
}

void udelay_masked(unsigned long usec)
{
	ulong tmo;

	tmo = usec / 1000;
	tmo *= (timer_load_val * 100);
	tmo /= 1000;

	reset_timer_masked();

	while (get_timer_masked() < tmo)
		/*NOP*/;
}

/*
 * This function is derived from PowerPC code (read timebase as long long).
 * On ARM it just returns the timer value.
 */
unsigned long long get_ticks(void)
{
	return get_timer(0);
}

/*
 * This function is derived from PowerPC code (timebase clock frequency).
 * On ARM it returns the number of timer ticks per second.
 */
ulong get_tbclk(void)
{
	ulong tbclk;

	tbclk = timer_load_val * 100;
	return tbclk;
}


#endif  /* defined(CONFIG_EP93XX) */
