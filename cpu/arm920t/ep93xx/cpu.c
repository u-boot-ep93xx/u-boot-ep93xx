/* vim: set ts=8 sw=8 noet:
 *
 * Cirrus Logic EP93xx CPU-specific support.
 *
 * Copyright (C) 2004, 2005
 * Cory T. Tusar, Videon Central, Inc., <ctusar@videon-central.com>
 *
 * See file CREDITS for list of people who contributed to this project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <common.h>

#if defined(CONFIG_EP93XX)
#include <ep93xx.h>


/* WARNING!  WARNING!  WARNING!
 *
 * The EP93xx family supposedly has support for chip type (i.e. 9301, 9302,
 * 9312, etc.) and silicon revision determination.  In practice, however,
 * run-time determination of chip type via documented methods has proven
 * impossible on all chip revisions (through E0) thus far due to silicon
 * 'issues'.
 *
 * Perhaps this will change in the future; until then, use the autodetected
 * chip type at your own risk!
 *
 * Note also that the chip revision indication found in the CHIP_ID register
 * (0x80930094) has proven reliable for determining a particular core's
 * silicon version.
 *
 * WARNING!  WARNING!  WARNING! */
extern int checkcpu(void)
{
       printf("CPU: Cirrus Logic ");
       switch (SecurityExtensionID & 0x000001FE) {
               case 0x00000008:
                       printf("EP9301");
                       break;

               case 0x00000004:
                       printf("EP9312");
                       break;

               case 0x00000000:
                       printf("EP9315");
                       break;

               default:
                       printf("<unknown>");
                       break;
       }

       printf(" - Rev. ");
       switch (SysconCHIP_ID & 0xF0000000) {
               case 0x00000000:
                       printf("A");
                       break;

               case 0x10000000:
                       printf("B");
                       break;

               case 0x20000000:
                       printf("C");
                       break;

               case 0x30000000:
                       printf("D0");
                       break;

               case 0x40000000:
                       printf("D1");
                       break;

               case 0x50000000:
                       printf("E0");
                       break;

               default:
                       printf("?");
                       break;
       }
       printf(" (%.8x/%.8x)\n", SecurityExtensionID, SysconCHIP_ID);

       return(0);
}


/* All EP93xx variants have 16 KiB I-cache. */
extern int checkicache(void)
{
       return(16 << 10);
}


/* All EP93xx variants have 16 KiB D-cache. */
extern int checkdcache(void)
{
       return(16 << 10);
}


/* This is a nop on ARM, and is included here for completeness only. */
extern void upmconfig(unsigned int upm, unsigned int *table, unsigned int size)
{
       /* nop */
}


/* We reset the CPU by generating a 1-->0 transition on DeviceCfg bit 31. */
extern void reset_cpu(ulong addr)
{
       /* Unlock DeviceCfg and write '1' */
       SYSCON_SW_UNCLOCK;
       SysconDEVCFG |= (1 << 31);

       /* Unlock DeviceCfg and write '0' */
       SYSCON_SW_UNCLOCK;
       SysconDEVCFG &= ~(1 << 31);

       /* Dying... */
       while(1);
}


#endif  /* defined(CONFIG_EP93XX) */
