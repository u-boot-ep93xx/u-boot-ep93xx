/* vim: set ts=8 sw=8 noet: */
/*
 * Configuration settings for Cirrus Logic EDB93xx boards.
 *
 * Copyright (C) 2004, 2005
 * Cory T. Tusar, Videon Central, Inc., <ctusar@videon-central.com>
 *
 * Original EDB93xx port done by llandre <r&d2@dave-tech.it>
 *
 * Based on the settings found in smdk2410.h, which is
 *
 * (C) Copyright 2002
 * Sysgo Real-Time Solutions, GmbH <www.elinos.com>
 * Marius Groeger <mgroeger@sysgo.de>
 * Gary Jennejohn <gj@denx.de>
 * David Mueller <d.mueller@elsoft.ch>
 *
 * See file CREDITS for list of people who contributed to this project.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#if !defined(EDB93XX_H)
#define EDB93XX_H


/* -----------------------------------------------------------------------------
 * Initial environment and monitor configuration options.
 *
 * These should be set appropriately for your particular development
 * environment.  Refer to the README for more detailed descriptions.
 */
#define CONFIG_ETHADDR                 08:00:3E:26:0A:5B
#define CONFIG_NETMASK                 255.255.255.0
#define CONFIG_IPADDR                  192.168.128.224
#define CONFIG_SERVERIP                192.168.128.181
#define CONFIG_GATEWAYIP               192.168.4.1

#define CONFIG_BOOTDELAY               1
#define CONFIG_CMDLINE_TAG             1
#define CONFIG_INITRD_TAG              1
#define CONFIG_SETUP_MEMORY_TAGS       1
#define CONFIG_BOOTARGS                "root=/dev/nfs console=ttyAM0,115200 ip=dhcp"
#define CONFIG_BOOTFILE                "edb93xx.img"
//#define CONFIG_BOOTCOMMAND             "bootm"


/* -----------------------------------------------------------------------------
 * Do NOT touch anything below this point unless you know what you are doing!
 */

/* -----------------------------------------------------------------------------
 * High level configuration options
 */

/* This is an ARM920T core, in a Cirrus Logic 93xx SoC, on a Cirrus Logic
 * EDB93xx board.  Note that 'make' will set the proper CONFIG_EP93<nn> and
 * CONFIG_EDB93<nn> symbols. */
#define CONFIG_ARM920T
#define CONFIG_EP93XX

/* input clock of PLL */
#define CONFIG_SYS_CLK_FREQ	3686400 /* the EP9312 has 3.6864MHz input clock */

/* Timer 3 set for 2KHz operation */
#define	CONFIG_SYS_HZ			2048
#undef  CONFIG_SYS_CLKS_IN_HZ		/* everything, incl board info, in Hz */


/* #define USE_920T_MMU		1*/
#undef CONFIG_USE_IRQ			/* we don't need IRQ/FIQ stuff */


/* -----------------------------------------------------------------------------
 * Monitor configuration
 *
 * Define the set of commands supported for the various EDB93xx boards, as
 * well as the default monitor prompt and amount of help available.
 */
#if defined(USE_920T_MMU)
#define CONFIG_COMMANDS			(CONFIG_CMD_DFL | \
					 CONFIG_SYS_CMD_SDRAM | \
					 CONFIG_SYS_CMD_JFFS2 | \
					 CONFIG_SYS_CMD_DHCP | \
					 CONFIG_SYS_CMD_PING | \
					 CONFIG_SYS_CMD_MII)

#else
#define CONFIG_COMMANDS			(CONFIG_CMD_DFL | \
					 CONFIG_SYS_CMD_SDRAM | \
					 CONFIG_SYS_CMD_JFFS2 | \
					 CONFIG_SYS_CMD_DHCP | \
					 CONFIG_SYS_CMD_PING | \
					 CONFIG_SYS_CMD_MII & \
					 ~CONFIG_SYS_CMD_CACHE)
#endif  /* defined(USE_920T_MMU) */

#include <config_cmd_default.h>


#if defined(CONFIG_EDB9301)
#define CONFIG_SYS_PROMPT			"EDB9301> "

#elif defined(CONFIG_EDB9302)
#define CONFIG_SYS_PROMPT			"EDB9302> "

#elif defined(CONFIG_EDB9307)
#define CONFIG_SYS_PROMPT			"EDB9307> "

#elif defined(CONFIG_EDB9312)
#define CONFIG_SYS_PROMPT			"EDB9312> "

#elif defined(CONFIG_EDB9315)
#define CONFIG_SYS_PROMPT			"EDB9315> "

#else
#define CONFIG_SYS_PROMPT			"EDB93xx> "
#endif  /* defined(CONFIG_EDB93nn) */

/* Enable "long" help in the monitor */
#define	CONFIG_SYS_LONGHELP

#define	CONFIG_SYS_CBSIZE		1024		/* Console I/O Buffer Size	*/
#define	CONFIG_SYS_PBSIZE (CONFIG_SYS_CBSIZE+sizeof(CONFIG_SYS_PROMPT)+16) /* Print Buffer Size */
#define	CONFIG_SYS_MAXARGS		16		/* max number of command args	*/
#define CONFIG_SYS_BARGSIZE		CONFIG_SYS_CBSIZE	/* Boot Argument Buffer Size	*/


#define	CONFIG_SYS_LOAD_ADDR		0x01000000	/* default load address	*/


/* -----------------------------------------------------------------------------
 * Serial port hardware configuration
 *
 * We default to using serial port 1 at 115200 baud.
 * TODO:  kgdb support has NOT been verified as functional.
 */
#define CONFIG_SERIAL1          	(1)
#define CONFIG_BAUDRATE			(115200)
#define CONFIG_SYS_BAUDRATE_TABLE		{ 9600, 19200, 38400, 57600, 115200 }

#if 0
#if (CONFIG_COMMANDS & CONFIG_SYS_CMD_KGDB)
#define CONFIG_KGDB_SER_INDEX		(1)
#define CONFIG_KGDB_BAUDRATE		(115200)
#endif
#endif


/* -----------------------------------------------------------------------------
 * Network hardware configuration
 *
 * We use the internal EP93xx MAC with and external MII compliant PHY.
 * TODO:  netconsole support has NOT been verified as functional.
 */
#if (defined(CONFIG_EDB9301) || defined(CONFIG_EDB9302) || \
		defined(CONFIG_EDB9307) || defined(CONFIG_EDB9312) || \
		defined(CONFIG_EDB9315))
#define CONFIG_DRIVER_EP93XX_MAC
#define CONFIG_MII_SUPPRESS_PREAMBLE
#define CONFIG_MII
#define CONFIG_PHY_ADDR			(1)

#undef CONFIG_NETCONSOLE

#endif  /* defined(CONFIG_EDB93nn) */


/* -----------------------------------------------------------------------------
 * Physical memory map
 *
 * The EDB9301 and EDB9302 have 1 bank of SDRAM at 0x00000000 consisting of
 * 1x Samsung K4S561632E-TC75 256 Mbit SDRAM on a 16-bit data bus, for a total
 * of 32 MB of SDRAM.
 *
 * The EDB9307, EDB9312, and EDB9315 have 1 bank of SDRAM at 0x00000000
 * consisting of 2x Samsung K4S561632E-TC75 256 Mbit SDRAMs on a 32-bit data
 * bus, for a total of 64 MB of SDRAM.
 */
#if (defined(CONFIG_EDB9301) || defined(CONFIG_EDB9302))
#define CONFIG_NR_DRAM_BANKS           (1)
#define PHYS_SDRAM_1                   (0x00000000)
#define PHYS_SDRAM_SIZE_1              (0x02000000)

#define CONFIG_SYS_MEMTEST_START              (0x00000000)
#define CONFIG_SYS_MEMTEST_END                        (0x01F00000)

#elif (defined(CONFIG_EDB9307) || defined(CONFIG_EDB9312) || \
               defined(CONFIG_EDB9315))
//#define CONFIG_NR_DRAM_BANKS           (1)
#define CONFIG_NR_DRAM_BANKS           (2)
#define PHYS_SDRAM_1                   (0x00000000)
#define PHYS_SDRAM_SIZE_1              (0x02000000)
#define PHYS_SDRAM_2                   (0x04000000)
#define PHYS_SDRAM_SIZE_2              (0x02000000)

#define CONFIG_SYS_MEMTEST_START              (0x00000000)
#define CONFIG_SYS_MEMTEST_END                        (0x03F00000)

#endif  /* defined(CONFIG_EDB93nn) */


/* Address in RAM to which boot parameters will be copied from the environment.
 * This must match the kernel is expecting. */
#define CONFIG_BOOT_PARAM_ADDR         (PHYS_SDRAM_1 + 0x100)


/* -----------------------------------------------------------------------------
 * Run-time memory allocations
 *
 * The global data area size (must be > sizeof(gd_t)), stack sizes, and heap
 * size are set using the values below.
 */
#define CONFIG_SYS_GBL_DATA_SIZE		(128)

#define CONFIG_STACKSIZE		(128 * 1024)

#if defined(CONFIG_USE_IRQ)
#define CONFIG_STACKSIZE_IRQ		(4 * 1024)
#define CONFIG_STACKSIZE_FIQ		(4 * 1024)
#endif  /* defined(CONFIG_USE_IRQ) */

#define CONFIG_SYS_MALLOC_LEN			(512 * 1024)


/* -----------------------------------------------------------------------------
 * FLASH and environment organization
 *
 * The EDB9301 and EDB9302 have 1 bank of flash memory at 0x60000000 consisting
 * of 1x Intel TE28F128J3C-150 128 Mbit flash on a 16-bit data bus, for a total
 * of 16 MB of CFI-compatible flash.
 *
 * The EDB9307, EDB9312, and EDB9315 have 1 bank of flash memory at 0x60000000
 * consisting of 2x Micron MT28F128J3-12 128 Mbit flash on a 32-bit data bus,
 * for a total of 32 MB of CFI-compatible flash.
 *
 *                            EDB9301/02             EDB9307/12/15
 * 0x00000000 - 0x0003FFFF    u-boot                 u-boot
 * 0x00040000 - 0x0005FFFF    environment #1         environment #1
 * 0x00060000 - 0x0007FFFF    unused                 environment #1 (continued)
 * 0x00080000 - 0x0009FFFF    environment #2         environment #2
 * 0x000A0000 - 0x000BFFFF    unused                 environment #2 (continued)
 * 0x000C0000 - 0x000FFFFF    unused                 unused
 * 0x00100000 - 0x002FFFFF    kernel image #1        kernel image #1
 * 0x00300000 - 0x004FFFFF    kernel image #2        kernel image #2
 * 0x00500000 - 0x00FFFFFF    JFFS2                  JFFS2
 * 0x01000000 - 0x01FFFFFF    not present            JFFS2 (continued)
 */
#if (defined(CONFIG_EDB9301) || defined(CONFIG_EDB9302))
#define CONFIG_SYS_FLASH_CFI			1
#define CONFIG_FLASH_CFI
#define CONFIG_FLASH_CFI_DRIVER
#define CONFIG_SYS_MAX_FLASH_BANKS		(1)
#define CONFIG_SYS_MAX_FLASH_SECT		(128)

#define PHYS_FLASH_1			(0x60000000)
#define CONFIG_SYS_FLASH_BASE			(PHYS_FLASH_1)
#define CONFIG_SYS_MONITOR_BASE		(CONFIG_FLASH_BASE)
#define CONFIG_SYS_MONITOR_LEN			(256 * 1024)

#define CONFIG_ENV_OVERWRITE		/* Vendor parameters are unprotected */
#define CONFIG_ENV_IS_IN_FLASH
#define CONFIG_ENV_ADDR			(0x60040000)
#define CONFIG_ENV_ADDR_REDUND		(0x60080000)
#define CONFIG_ENV_SECT_SIZE		(0x00020000)
#define CONFIG_ENV_SIZE			(CONFIG_ENV_SECT_SIZE)
#define CONFIG_ENV_SIZE_REDUND		(CONFIG_ENV_SECT_SIZE)

#define CONFIG_SYS_JFFS2_FIRST_BANK		(0)
#define CONFIG_SYS_JFFS2_FIRST_SECTOR		(28)
#define CONFIG_SYS_JFFS2_NUM_BANKS		(1)

#elif (defined(CONFIG_EDB9307) || defined(CONFIG_EDB9312) || \
		defined(CONFIG_EDB9315))
#define CONFIG_SYS_FLASH_CFI			1
#define CONFIG_FLASH_CFI
#define CONFIG_FLASH_CFI_DRIVER
#define CONFIG_SYS_MAX_FLASH_BANKS		(1)
#define CONFIG_SYS_MAX_FLASH_SECT		(128)

#define PHYS_FLASH_1			(0x60000000)
#define CONFIG_SYS_FLASH_BASE			(PHYS_FLASH_1)
#define CONFIG_SYS_MONITOR_BASE		(CONFIG_FLASH_BASE)
#define CONFIG_SYS_MONITOR_LEN			(256 * 1024)

#define CONFIG_ENV_OVERWRITE		/* Vendor parameters are unprotected */
#define CONFIG_ENV_IS_IN_FLASH
#define CONFIG_ENV_ADDR			(0x60040000)
#define CONFIG_ENV_ADDR_REDUND		(0x60080000)
#define CONFIG_ENV_SECT_SIZE		(0x00040000)
#define CONFIG_ENV_SIZE			(CONFIG_ENV_SECT_SIZE)
#define CONFIG_ENV_SIZE_REDUND		(CONFIG_ENV_SECT_SIZE)

#define CONFIG_SYS_JFFS2_FIRST_BANK		(0)
#define CONFIG_SYS_JFFS2_FIRST_SECTOR		(14)
#define CONFIG_SYS_JFFS2_NUM_BANKS		(1)

#endif  /* defined(CONFIG_EDB93nn) */


#endif  /* !defined(EDB93XX_H) */

