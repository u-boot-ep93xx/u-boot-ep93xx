#
# (C) Copyright 2002-2003
# Adam Bezanson, Network Audio Technologies, Inc., <bezanson@netaudiotech.com>
#
# Cirrus Logic board for Cirrus Logic EP9312 Rev D. (ARM920T) cpu
#
#
#
##################################################
# Assumed configuration for EDB9312 revision 2
# Internal boot
# SDRAM chip select: /SDCS3
# SDRAM size: 64 MB
# Sync Memory Boot
# 
# We place the stuff close to the SDRAM upper bound
TEXT_BASE = 0x05f00000
