/* vim: set ts=8 sw=8 noet: */
/*
 * (C) Copyright 2002 2003
 * Network Audio Technologies, Inc. <www.netaudiotech.com>
 * Adam Bezanson <bezanson@netaudiotech.com>
 *
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include <common.h>
#include <ep93xx.h>
#include <linux/byteorder/swab.h>

#define HAL_WRITE_UINT32( _register_, _value_ ) \
        (*((volatile unsigned long *)(_register_)) = (_value_))

/* ------------------------------------------------------------------------- */

/*
 * static inline void delay (unsigned long loops)
 * {
 * 	__asm__ volatile ("1:\n"
 * 	  "subs %0, %1, #1\n"
 * 	  "bne 1b":"=r" (loops):"0" (loops));
 * }
 */

/*
 * Miscellaneous platform dependent initialisations
 */


int board_init (void)
{
	DECLARE_GLOBAL_DATA_PTR;
    unsigned long ClkSet1;
    unsigned long ClkSet2;

    //
    // TODO remove this hack.  This is to set the wait states to max for Flash.
    //
#ifdef CONFIG_EP9301
    HAL_WRITE_UINT32(0x80080018, 0x1000FFFF);
#else
    HAL_WRITE_UINT32(0x80080018, 0x2000FFFF);
#endif // CONFIG_EP9301

    //
    // Set the output of PLL2 to 192Mhz
    //
    ClkSet2 = 0x300dc317;

    // hvr
    ClkSet2 = 0x700cc317;

#ifdef CONFIG_EP9301
    //
    // Set the output of the PLL to 332Mhz
    //
    ClkSet1 = EP9312_CLKSET1_NBYP | 0x00fa5a;

    //
    // Set the FCLKDIV value to divide by 2 (166Mhz).
    //
    ClkSet1 |= (1 << EP9312_CLKSET1_FCLKDIV_SHIFT);

    //
    // Set the HCLKDIV value to divide by 5 (66Mhz).
    //
    ClkSet1 |= (3 << EP9312_CLKSET1_HCLKDIV_SHIFT);

    // hvr
    ClkSet1 = 0x02b4fa5a;
#else
    //
    // Set the output of the PLL to 400Mhz
    //
    ClkSet1 = EP9312_CLKSET1_NBYP | 0x00e39e;

    //
    // Set the FCLK to 400/2 or 200Mhz
    //
    ClkSet1 |= (1 << EP9312_CLKSET1_FCLKDIV_SHIFT);

    //
    // Set the HCLK to 400/4 or 100Mhz
    //
    ClkSet1 |= (2 << EP9312_CLKSET1_HCLKDIV_SHIFT);

    // hvr
    ClkSet1 = 0x02a4e39e;
#endif // CONFIG_EP9301

    //
    // Set PCLKDIV so that PCLK = HCLK / 2
    //
    //hvr (commented out)
    //ClkSet1 |= (1 << EP9312_CLKSET1_PCLKDIV_SHIFT);

    HAL_WRITE_UINT32(EP9312_CLKSET1, ClkSet1);

    //
    // Do the five required nops to keep us clean.
    //
    __asm ("nop");
    __asm ("nop");
    __asm ("nop");
    __asm ("nop");
    __asm ("nop");

    //
    // Write out the value to ClkSet 2
    //
    HAL_WRITE_UINT32(EP9312_CLKSET2, ClkSet2);

    //
    // Go to Async mode
    //
    __asm ("mrc p15, 0, r0, c1, c0, 0");
    __asm ("orr r0, r0, #0xc0000000");
    __asm ("mcr p15, 0, r0, c1, c0, 0");

		icache_enable();

#ifdef USE_920T_MMU
		dcache_enable();
#endif

    //
    // Set this bit like the Kernel expects it(runs serial off of the 14Mhz).
    //
    HAL_WRITE_UINT32(EP9312_PWRCNT, EP9312_PWRCNT_UARTBAUD);

       /* Machine number, as defined in linux/arch/arm/tools/mach-types */
#if defined(CONFIG_EDB9301)
       gd->bd->bi_arch_number = 462;
       
#elif defined(CONFIG_EDB9302)
       gd->bd->bi_arch_number = 538;
       
#elif defined(CONFIG_EDB9307)
       gd->bd->bi_arch_number = 607;
       
#elif defined(CONFIG_EDB9312)
       gd->bd->bi_arch_number = 451;
       
#elif defined(CONFIG_EDB9315)
       gd->bd->bi_arch_number = 463;

#else
#error "CONFIG_EDB93nn is not properly #define'd"
#endif

       /* adress of boot parameters */
       gd->bd->bi_boot_params = CONFIG_BOOT_PARAM_ADDR;

       /* We have a console */
       gd->have_console = 1;

       return(0);
}

int dram_init (void)
{
       DECLARE_GLOBAL_DATA_PTR;

#if CONFIG_NR_DRAM_BANKS>0
       gd->bd->bi_dram[0].start = PHYS_SDRAM_1;
       gd->bd->bi_dram[0].size = PHYS_SDRAM_SIZE_1;
#else
# error bad bank config
#endif

#if CONFIG_NR_DRAM_BANKS>1
       gd->bd->bi_dram[1].start = PHYS_SDRAM_2;
       gd->bd->bi_dram[1].size = PHYS_SDRAM_SIZE_2;
#endif

#if CONFIG_NR_DRAM_BANKS>2
# error bad bank config
#endif
       
       return(0);
}
